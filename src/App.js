import React, { useState, useEffect } from 'react';

// Components
import Form from './components/Form';
import ListNotes from './components/ListNotes';
// local storage
import { DATA_WORDS }  from './utils/constants';

// styles 
import './App.scss';

function App() {

    // states 
    const [allWords, setWords] = useState([]);
    const [realodData, setReloadData] = useState(false);
    
    // Use effect hook
    useEffect(() => {
      const words = localStorage.getItem(DATA_WORDS);
      const dataPreparated = JSON.parse(words); 
      setWords(dataPreparated);
      setReloadData(false)
    },[realodData])

  // Functions 
  const save_word = (event,details) => {
    event.preventDefault();
    // Destructuring 
    const { word, definition} = details;

    let dataComplete;
    allWords ? dataComplete = allWords : dataComplete = [];

    if(word && definition){
      dataComplete.push(details)
      localStorage.setItem(DATA_WORDS,JSON.stringify(dataComplete))
      setReloadData(true);
    }else{
      console.error('The word and the definition can not be null');
    }
    // -- 
    dataComplete = [];
  }
  
  const delete_note = (index) => {
    allWords.splice(index,1);
    setWords(allWords);
    localStorage.setItem(DATA_WORDS,JSON.stringify(allWords));
    setReloadData(true);
  }

  return (
    <div className="App-main-container">
      <Form save={save_word} data={allWords}/>
      <ListNotes notes={allWords} delete_note={delete_note}/>
    </div>
  );
}

export default App;
