import React from 'react';
import Paper from '@material-ui/core/Paper';

// Styles 
import './Note.scss';

export default function Note(props){

    const { data:{word, definition, emoji}, onDelete, index } = props;

    return(
        <>
            <Paper> 
                <div className="container-content">
                    <div className="paper-head">
                        <h2 className="title">{word}</h2>
                        <i className="fas fa-times-circle icon-delete" onClick={() => onDelete(index)}></i>
                    </div>
                    <div className="paper-body">
                        <small className="txt-secondary">Definition</small>
                        <p>{definition}</p>
                    </div>
                    <div className="paper-footer">
                        <i className={`fa ${emoji}`}></i>
                    </div>
                </div>
            </Paper>
        </>
    )
}