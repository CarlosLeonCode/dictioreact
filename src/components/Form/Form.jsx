import React, { useState } from 'react';

// Components 
import { Card, 
        CardContent, 
        FormControl, 
        FormGroup, 
        TextField, 
        Button, 
        Radio, 
        RadioGroup,
        FormControlLabel,
        FormLabel }  from '@material-ui/core';
import { Dictionary } from '../SVG/SVG' 


// Styles 
import './Form.scss';

export default function Form(props){

    // Props 
    const { save } = props;

    // State 
    const [wordDetails, setDetailsWord] = useState( {
        word: null,
        definition: null,
        emoji: null
    })

    // functions 
    const onChangeData = (event) => {
        event.preventDefault();
        // Update states 
        setDetailsWord({
            ...wordDetails,                             // Recover data from state
            [event.target.name]: event.target.value     // Upda only a value
        })
    }


    return(
        <div className="container-form">
            <Card className="card">
                <CardContent className="card-content">
                    <div>
                        <Dictionary />
                    </div>
                    
                    {/* Form  */}
                    <div className="form-wrapper">
                        <form 
                            onSubmit={event => save(event,wordDetails)}
                            onChange={onChangeData}
                        >
                            <FormControl className="wrapper-content">

                                {/* Word  */}
                                <FormGroup>
                                    <TextField 
                                        type="text"
                                        name="word"
                                        placeholder="Write one word..."
                                        margin="normal"
                                        className="text-field-form"
                                        id="outlined-basic" 
                                        label="Word" 
                                        variant="outlined" 
                                    />
                                </FormGroup>

                                {/* Definition  */}
                                <FormGroup>
                                    <TextField 
                                        multiline
                                        name="definition"
                                        className="text-field-form"
                                        placeholder="Write definition"
                                        margin="normal"
                                        rows="4"
                                        id="outlined-multiline-static"
                                        label="Definition" 
                                        variant="outlined"
                                    />
                                </FormGroup>    

                                {/* Emojis  */}
                                <div className="wrapper-emojis">
                                    <FormLabel component="legend">Emoji</FormLabel>
                                    <RadioGroup aria-label="icon" className="grid-emojis">
                                        <FormControlLabel name="emoji" value="fa-surprise" control={<Radio />} label={<i className="far fa-surprise"></i>} />
                                        <FormControlLabel name="emoji" value="fa-tired" control={<Radio />} label={<i className="far fa-tired"></i>} />
                                        <FormControlLabel name="emoji" value="fa-smile-beam" control={<Radio />} label={<i className="far fa-smile-beam"></i>} />
                                        <FormControlLabel name="emoji" value="fa-laugh" control={<Radio />} label={<i className="far fa-laugh"></i>} />
                                        <FormControlLabel name="emoji" value="fa-frown" control={<Radio />} label={<i className="far fa-frown"></i>} />
                                        <FormControlLabel name="emoji" value="fa-grin-hearts" control={<Radio />} label={<i className="far fa-grin-hearts"></i>} />
                                    </RadioGroup>
                                </div>

                                {/* Button submit  */}
                                <FormGroup>
                                    <Button type="submit" 
                                            variant="outlined" 
                                            color="primary"> 
                                        Save <i className="far fa-paper-plane"></i> 
                                    </Button>
                                </FormGroup>

                            </FormControl>
                        </form>
                    </div>  
                </CardContent>
            </Card>
        </div>
    )
}