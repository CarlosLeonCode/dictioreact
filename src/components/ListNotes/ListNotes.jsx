import React from 'react';
import Note from '../Note';

// styles 
import './ListNotes.scss';

export default function ListNotes(props){

    const { notes, delete_note } = props;
    
    
    if(notes && notes.length > 0){
        return(
            <div className="container-list-notes">
           
                <div className="title">
                    <h2>Your words</h2>
                </div>
    
                <div className="container-notes">
                    { notes.map((data,index) => (
                        <Note data={data} key={index} onDelete={delete_note} index={index}/>
                    )) }
                </div>
            </div>
        )
    }else{
        return(
            <div className="container-list-notes">
                <h1 className="my-title-empty">Without words</h1>
            </div>
        )
    }

}